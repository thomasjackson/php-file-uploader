<?
require("settings.php");

// Upload the file
if($_GET["p"]=="go"){
	$filename = str_replace(" ", "_", basename($_FILES['file']['name']));

	// get the extension and check it is allowed
	$parts = explode(".", $filename);
	$extension = strtolower($parts[count($parts)-1]);
	if(in_array($extension, $disallowed_extensions)){
		$response = '<font color="red"><B>Error: Disallowed file extension type</b></font><br><br>';
	}
	else {
		$result = mysql_query("SELECT * FROM files_data WHERE file_name = '".$filename."' AND deleted = 0");
		if(mysql_num_rows($result)>0){
			$filename = time().'_'.$filename;
		}

		$uploadfile = $uploaddir.$filename;

		if($_FILES['file']['error']==4){
			$response = '<font color="red"><B>Error: No file selected for upload.</b></font><br><br>';
		}
		else if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
			$response = 'File is valid, and was successfully uploaded. Copy the following link into an email:<Br>';
			$response .= '<h2>'.$baseurl.$filename.'</h2>';
			$response .= '<font color="red">Please note: this file will be automatically removed from this facility in 14 days</font><br><br>';
			
			// If the user isn't on the inside of the network, let an admin know
			if(!(ip2long($_SERVER["REMOTE_ADDR"])>=ip2long($iprange_start)&&ip2long($_SERVER["REMOTE_ADDR"])<=ip2long($iprange_end))){
				mail($it_alerts_address,'File uploaded to file sharing platform',"A new file has been uploaded to the file sharing platform\n\n".$baseurl.$filename."\n\nBy ".$_SERVER["REMOTE_ADDR"]);
			}
			if(!mysql_query("INSERT INTO files_data (file_name, uploading_ip) VALUES ('".$filename."','".$_SERVER["REMOTE_ADDR"]."')")){
				$response = '<font color="red"><B>Error: Your file was not uploaded due to some issue with the database. Please contact IT.</b></font><br><br>';
			}
			chmod($uploadfile, 0666);
		} else {
			$response = '<font color="red"><B>Error: Your file was not uploaded due to an unknown error. Please contact IT.</b></font><br><br>';
		}
	}
}

?>

<html>
<body BGCOLOR="#FFFFFF" leftMargin=0 topMargin=0 rightMargin=0 marginheight="0" marginwidth="0">
<center>
<table BORDER="0" WIDTH="100%" CELLSPACING="0">
  <tr>
    <td BGCOLOR="#025CCA" ALIGN="CENTER">
    <hr SIZE="1" COLOR="#FFFFFF">
    <table>
      <tr>
        <td><font SIZE="3" COLOR="#FFFFFF" FACE="Helvetica, Arial"><b>File Uploader</b></font></td>
      </tr>
    </table>
    <hr SIZE="1" COLOR="#FFFFFF">
    </td>
  </tr>
  <tr>
    <td BGCOLOR="#FFFFFF">
          <center>
              <p><font COLOR="#000000" SIZE="2" FACE="Helvetica, Arial"><br>
                        <? 
if(isset($response)){
	echo $response; 
}
?> 

Welcome to the file uploader. This facility is designed for sending files that are too large to be sent via email.
<br><br>              
<form action="index.php?p=go" method="post" enctype="multipart/form-data">
<input type="file" name="file">
<input type="submit" value="Send" />
</form>

<br>
For questions or problems please contact IT support.</font></p>
      </center>
    </td>
  </tr>
</table>
</center>
</body>
</html>
