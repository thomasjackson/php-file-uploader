<?
// Set the directory on the server that we are uploading files to
// Make sure that your web server user has permissions to write to here
$uploaddir = '/my/web/directory/upload/'; // make sure there is a trailing / here

// This is the URL that is appended to the beginning of the file names
$baseurl = 'http://files.my.company.com/upload/'; // make sure there is a trailing / here

// This is a list of extensions that we don't want users to be able to upload
$disallowed_extensions = array('php','phtml');

// Define your internal IP subnet here, this will prevent alert emails from being sent from these IPs
$iprange_start = "192.168.0.0";
$iprange_end = "192.168.0.255";

// When a file is uploaded from an IP that is not contained in the above range, send an alert to this address
$it_alerts_address = "it.support@your.company.com";

// Set up the MySQL DB connection
mysql_connect("localhost", "user", "password");
mysql_select_db("files_db");

?>