CREATE TABLE IF NOT EXISTS `files_data` (
  `file_id` int(30) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(250) NOT NULL,
  `uploaded_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uploading_ip` varchar(128) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
